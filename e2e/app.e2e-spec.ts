import { SMETA2Page } from './app.po';

describe('smeta2 App', () => {
  let page: SMETA2Page;

  beforeEach(() => {
    page = new SMETA2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

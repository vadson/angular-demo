import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './core-components/main/main.component';
import { NgModule } from '@angular/core';
import { VisitHistoryComponent } from './core-components/visit-history/visit-history.component';
import { SecurityService } from './services/security.service';
import { LogoutComponent } from './core-components/logout/logout.component';
import { HashCalcComponent } from './core-components/hash-calc/hash-calc.component';
import { Page1Component } from './components/page1/page1.component';

const ROUTES: Routes = [
    {
        path: 'main', component: MainComponent,
        children: [
            { path: 'page1', component: Page1Component },
            { path: 'visits', component: VisitHistoryComponent, canActivate: [SecurityService] },
            { path: 'logout', component: LogoutComponent },
            { path: 'hash', component: HashCalcComponent, canActivate: [SecurityService] }
        ]
    },
    { path: '**', redirectTo: 'main/page1' },
];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES, { useHash: true })],
    exports: [RouterModule],
    providers: [SecurityService]
})

export class AppRoutingModule { }


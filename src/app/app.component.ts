import { Component, ViewContainerRef, OnInit, ViewChild } from '@angular/core';
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
import { SecurityService } from './services/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;

  constructor(
    private viewRef: ViewContainerRef,
    private toast: ToastrService,
    private _security: SecurityService
  ) {
  }

  ngOnInit() {
    this.toast.overlayContainer = this.toastContainer;
    this._security.getVisitCount();
  }

  getUserName() {
    return this._security.getCurrentUserName();
  }

  getVisitCount() {
    return this._security.visitCount;
  }

}

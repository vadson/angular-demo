import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { MainComponent } from './core-components/main/main.component';
import { AppRoutingModule } from './app-routing.module';
import { RESTService } from './services/rest.service';
import { ToastrModule } from 'ngx-toastr';
import { VisitHistoryComponent } from './core-components/visit-history/visit-history.component';
import { ConfirmDialogComponent } from './core-components/confirm-dialog/confirm-dialog.component';
import { SpinnerService } from './services/spinner.service';
import { LogoutComponent } from './core-components/logout/logout.component';
import { FooterComponent } from './core-components/footer/footer.component';
import { HashCalcComponent } from './core-components/hash-calc/hash-calc.component';
import { Page1Component } from './components/page1/page1.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    VisitHistoryComponent,
    ConfirmDialogComponent,
    LogoutComponent,
    FooterComponent,
    HashCalcComponent,
    Page1Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [RESTService, SpinnerService],
  bootstrap: [AppComponent]
})
export class AppModule { }

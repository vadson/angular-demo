import { Injectable } from '@angular/core';
import { RESTService } from './rest.service';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

export const SecuredRoutesAccessLevels: { route: string; level: number }[] = [
  { route: 'main/visits', level: 2 },
  { route: 'main/hash', level: 2 }
];

@Injectable()
export class SecurityService implements CanActivate {
  private currentAccessLevel: number;
  private accessLevelPromise: Promise<number>;

  private currentUserName: string;
  private userNamePromise: Promise<string>;

  public visitCount: number;

  constructor(
    private _rest: RESTService,
    private router: Router,
    private _toast: ToastrService
  ) {
    this.currentAccessLevel = 0;
    this.accessLevelPromise = null;
    this.currentUserName = '';
    this.userNamePromise = null;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    let res: Promise<boolean>;

    const requiredLevel = this.getAccessLevelForRoute(state.url);
    if (requiredLevel) {
      res = this.getCurrentAccessLevel()
        .then(data => {
          if (data >= requiredLevel) {
            return true;
          } else {
            this.router.navigate(['/']);
            return false;
          }
        })
        .catch(() => {
          this.router.navigate(['/']);
          return false;
        });
    } else {
      res = Promise.resolve(true);
    }
    return res;
  }

  getAccessLevelForRoute(route: string): number {
    if (route && route.substr(0, 1) === '/') {
      route = route.substr(1, route.length - 1);
    }
    const robj = SecuredRoutesAccessLevels.find(item => item.route === route);
    return robj ? robj.level : 0;
  }

  getCurrentAccessLevel(): Promise<number> {
    if (this.accessLevelPromise === null) {
      this.accessLevelPromise = this.getAccessLevel();
    }
    return this.accessLevelPromise;
    // } else {
    //   return Promise.resolve(this.currentAccessLevel);
    // }
  }

  getCurrentUserName(): string {
    return this.currentUserName;
  }

  logout = async () => {
    const data = await this._rest.postFormData(['security', 'logout']);
    this.accessLevelPromise = null;
    this.currentAccessLevel = 0;
    this.userNamePromise = null;
    this.currentUserName = '';
    this._rest.resetFirstPromise();
    return data;
  }

  getVisitCount() {
    this._rest
      .get(['activity', 'visitCount'])
      .then(data => (this.visitCount = Number(data.data)))
      .catch(() => (this.visitCount = undefined));
  }

  private getAccessLevel(): Promise<number> {
    return this._rest
      .postFormData(['security', 'accessLevel'])
      .then(data => {
        this.currentAccessLevel = Number(data.authority);
        this.currentUserName = data.user;
        this.visitCount = Number(data.visits);
        return this.currentAccessLevel;
      })
      .catch(() => {
        this.currentAccessLevel = 0;
        return this.currentAccessLevel;
      });
  }
}

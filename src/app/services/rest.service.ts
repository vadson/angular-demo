import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from './spinner.service';
import { BasicAuthHttpService } from './basic-auth-http.service';

export interface RestParams {
  [name: string]: any;
}

interface RestResponse {
  error: string;
  error_message: string;
  data: any;
}

@Injectable()
export class RESTService extends BasicAuthHttpService {
  constructor(
    httpCore: Http,
    private _toast: ToastrService,
    private _spinner: SpinnerService
  ) {
    super(httpCore);
  }

  get(method: string | string[], params?: RestParams): Promise<any> {
    this._spinner.showSpinner();

    return super
      .get(method, params)
      .then((response: Response) => this.processServerResponse(response))
      .catch((response: any) => this.processCommunicationError(response));
  }

  public post(
    method: string | string[],
    data?: any,
    restURL?: string
  ): Promise<any> {
    this._spinner.showSpinner();

    return super
      .post(method, data, restURL)
      .then((response: Response) => this.processServerResponse(response))
      .catch((response: any) => this.processCommunicationError(response));
  }

  public postFormData(method: string | string[], data?: any): Promise<any> {
    this._spinner.showSpinner();

    return super
      .postFormData(method, data)
      .then((response: Response) => this.processServerResponse(response))
      .catch((response: any) => this.processCommunicationError(response));
  }

  public postMultipart(
    method: string | string[],
    data?: any,
    restURL?: string
  ): Promise<any> {
    this._spinner.showSpinner();

    return super
      .postMultipart(method, data, restURL)
      .then((response: Response) => this.processServerResponse(response))
      .catch((response: any) => this.processCommunicationError(response));
  }

  public postFile(
    method: string | string[],
    fileHandler: File
  ): Promise<any> {
    return this.postMultipart(method, { file: fileHandler });
  }

  private processCommunicationError(response: any) {
    this._spinner.hideSpinner();
    console.log('Communication error: ' + response);
    this._toast.error(response, 'Server error');
    return Promise.reject(response);
  }

  private processServerResponse(response: Response): Promise<any> {
    this._spinner.hideSpinner();
    let res: Promise<any>;
    try {
      const body: RestResponse = response.json();
      if (body.error === 'Y') {
        res = Promise.reject(body.error_message);
      } else {
        res = Promise.resolve(body);
      }
    } catch (error) {
      res = Promise.reject('Empty response from server');
    }
    return res;
  }
}

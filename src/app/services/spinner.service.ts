import { Injectable } from '@angular/core';

const spinnerHtml = '<i aria-hidden="true" class="fa fa-spinner fa-2 fa-spin"></i>';

@Injectable()
export class SpinnerService {

  private spinnerElement: any;

  constructor() {
    this.spinnerElement = null;
  }

  showSpinner() {
    if (!this.spinnerElement) {
      this.spinnerElement = document.createElement('div');
      this.spinnerElement.innerHTML = spinnerHtml;
      this.spinnerElement.className = 'spinner';
      document.body.appendChild(this.spinnerElement);
    }
  }

  hideSpinner() {
    if (this.spinnerElement) {
      document.body.removeChild(this.spinnerElement);
      this.spinnerElement = null;
    }
  }

}

import { RestParams } from './rest.service';
import { Http, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { AnimationRendererFactory } from '@angular/platform-browser/animations/src/animation_renderer';

export const REST_URL = './rest/';
const PHP_REST_SUFFIX = '.php';

export class BasicAuthHttpService {
  private veryFirstPromise: Promise<any>;

  constructor(private http: Http) {
    this.veryFirstPromise = null;
  }

  resetFirstPromise() {
    this.veryFirstPromise = null;
  }

  transformMethodToPath(method: string | string[]): string {
    let restPathArr: string[];
    if (typeof method === 'string') {
      restPathArr = [method];
    } else if (Array.isArray(method)) {
      restPathArr = method;
    } else {
      restPathArr = [];
    }
    return restPathArr.join('/');
  }

  get(
    method: string | string[],
    params?: RestParams,
    restURL: string = REST_URL
  ): Promise<any> {
    restURL = restURL + this.transformMethodToPath(method) + PHP_REST_SUFFIX;
    const rqParams: URLSearchParams = new URLSearchParams();
    if (params) {
      for (const prop in params) {
        if (params.hasOwnProperty(prop)) {
          rqParams.set(prop, params[prop]);
        }
      }
    }

    if (this.veryFirstPromise) {
      return this.veryFirstPromise.then(() =>
        this.http.get(restURL, { search: rqParams }).toPromise()
      );
    } else {
      this.veryFirstPromise = this.http
        .get(restURL, { search: rqParams })
        .toPromise();
      return this.veryFirstPromise;
    }
  }

  public post(
    method: string | string[],
    data?: any,
    restURL: string = REST_URL
  ): Promise<any> {
    const headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8'
    });
    const requestOptions = new RequestOptions({ headers: headers });
    restURL = restURL + this.transformMethodToPath(method) + PHP_REST_SUFFIX;

    const postData = data || {};

    const body = JSON.stringify(postData);
    if (this.veryFirstPromise) {
      return this.veryFirstPromise.then(() =>
        this.http.post(restURL, body, requestOptions).toPromise()
      );
    } else {
      this.veryFirstPromise = this.http
        .post(restURL, body, requestOptions)
        .toPromise();
      return this.veryFirstPromise;
    }
  }

  public postMultipart(
    method: string | string[],
    data?: any,
    restURL: string = REST_URL
  ): Promise<any> {
    const headers = new Headers({});
    const requestOptions = new RequestOptions({ headers: headers });

    const postData = data || {};
    restURL = restURL + this.transformMethodToPath(method) + PHP_REST_SUFFIX;

    let body;
    body = new FormData();
    for (const p in postData) {
      if (postData.hasOwnProperty(p)) {
        body.append(p, postData[p]);
      }
    }
    if (this.veryFirstPromise) {
      return this.veryFirstPromise.then(() =>
        this.http.post(restURL, body, requestOptions).toPromise()
      );
    } else {
      this.veryFirstPromise = this.http
        .post(restURL, body, requestOptions)
        .toPromise();
      return this.veryFirstPromise;
    }
  }

  public postFormData(
    method: string | string[],
    data?: any,
    restURL: string = REST_URL
  ): Promise<any> {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    const requestOptions = new RequestOptions({ headers: headers });

    restURL = restURL + this.transformMethodToPath(method) + PHP_REST_SUFFIX;
    const formData = new URLSearchParams();
    if (data) {
      for (const prop in data) {
        if (data.hasOwnProperty(prop)) {
          formData.set(prop, data[prop]);
        }
      }
    }

    if (this.veryFirstPromise) {
      return this.veryFirstPromise.then(() =>
        this.http.post(restURL, formData, requestOptions).toPromise()
      );
    } else {
      this.veryFirstPromise = this.http
        .post(restURL, formData, requestOptions)
        .toPromise();
      return this.veryFirstPromise;
    }
  }
}

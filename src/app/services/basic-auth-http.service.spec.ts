import { TestBed, inject } from '@angular/core/testing';

import { BasicAuthHttpService } from './basic-auth-http.service';

describe('BasicAuthHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasicAuthHttpService]
    });
  });

  it('should ...', inject([BasicAuthHttpService], (service: BasicAuthHttpService) => {
    expect(service).toBeTruthy();
  }));
});

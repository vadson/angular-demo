import { Component, OnInit } from '@angular/core';
import { RESTService } from 'src/app/services/rest.service';

interface PiFaceState {
  status: string;
  in0: number;
  in1: number;
  in2: number;
  in3: number;
  in4: number;
  in5: number;
  in6: number;
  in7: number;
}

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
  message: string;

  constructor(private _rest: RESTService) {}

  ngOnInit() {
    this.loadData();
  }

  loadData = async () => {
    this.message = '';
    try {
      const resp = await this._rest.get('app/comp1Data');
      this.message = resp.data || 'No server data :(';
    } catch (e) {
      this.message = 'Ups! Error loading data: ' + e;
    }
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SecurityService } from '../../services/security.service';

interface MenuItem {
  name: string;
  route: string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  currentAccessLevel: Promise<number>;

  public menuItems: MenuItem[] = [
    { name: 'Опция', route: 'main/page1' },
    { name: 'Хеш калькулятор', route: 'main/hash' },
    { name: 'Журнал посещений', route: 'main/visits' }
  ];

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _toast: ToastrService,
    private _security: SecurityService
  ) {}

  ngOnInit() {
    this.currentAccessLevel = this._security.getCurrentAccessLevel();
  }

  onNavigate(item: MenuItem) {
    this.currentAccessLevel = this._security.getCurrentAccessLevel();
    this.doNavigate(item.route);
  }

  onLogout() {
    this.currentAccessLevel = Promise.resolve(0);
    this.doNavigate('main/logout').then(() => {
      this._security.logout();
    });
  }

  isCurrentPage(item: MenuItem) {
    return (
      this._route.snapshot.url[0].toString() +
        (this._route.snapshot.firstChild
          ? '/' + this._route.snapshot.firstChild.url[0].toString()
          : '') ===
      item.route
    );
  }

  getMenuItemAccessLevel(item: MenuItem): number {
    return this._security.getAccessLevelForRoute(item.route);
  }

  private doNavigate(url: string): Promise<any> {
    return this._router.navigate([url]).catch(error => {
      this._toast.error(error);
    });
  }
}

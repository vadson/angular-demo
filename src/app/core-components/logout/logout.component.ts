import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  showDialog: boolean;

  constructor() {
    this.showDialog = false;
  }

  ngOnInit() {
    this.showDialog = true;
    setTimeout(() => { this.showDialog = false; }, 5000);
  }
}

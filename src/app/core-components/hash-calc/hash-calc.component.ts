import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RESTService } from '../../services/rest.service';
import { Subject } from 'rxjs';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-hash-calc',
  templateUrl: './hash-calc.component.html',
  styleUrls: ['./hash-calc.component.css']
})
export class HashCalcComponent implements OnInit, OnDestroy {
  public password: string;
  public hash: string;

  passSubject: Subject<string> = new Subject();

  @ViewChild('hashArea') hashArea: ElementRef;
  constructor(private _toast: ToastrService, private _rest: RESTService) {}

  ngOnInit() {
    this.password = '';
    this.hash = '';
    this.passSubject
      .pipe(
        distinctUntilChanged(),
        debounceTime(500)
      )
      .subscribe(pass => {
        if (pass.length > 0) {
          this.onHashCalculate();
        }
      });
  }

  ngOnDestroy() {
    this.passSubject.unsubscribe();
  }

  onHashCalculate() {
    this.hash = '';
    if (this.password.length > 0) {
      this._rest
        .postFormData(['security', 'hashCalc'], { pass: this.password })
        .then(data => (this.hash = data.hash))
        .catch(() => {});
    } else {
      this._toast.error('Please enter password');
    }
  }

  onCopyHash() {
    this.hashArea.nativeElement.select();
    try {
      const success = document.execCommand('copy');
      if (!success) {
        this._toast.warning('Нажмите: Ctrl+C');
      } else {
        this._toast.success('Скопировано');
      }
    } catch (e) {
      this._toast.error('Ошибка копирования: ' + e);
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HashCalcComponent } from './hash-calc.component';

describe('HashCalcComponent', () => {
  let component: HashCalcComponent;
  let fixture: ComponentFixture<HashCalcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HashCalcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HashCalcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

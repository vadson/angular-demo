import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  @Input() public title: string;
  @Output() public confirm: EventEmitter<any> = new EventEmitter();
  @Output() public cancel: EventEmitter<any> = new EventEmitter();

  showDialog: boolean;

  constructor() {
    this.showDialog = false;
  }

  ngOnInit() {
    setTimeout(() => { this.showDialog = true; }, 100);
    setTimeout(() => { window.scrollBy(0, 1000); }, 120);
  }

  onConfirm() { this.showDialog = false;
    setTimeout(() => {
      this.confirm.emit();
    }, 500);
  }

  onCancel() {
    this.showDialog = false;
    setTimeout(() => {
      this.cancel.emit();
    }, 500);
  }

}

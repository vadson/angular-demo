import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RESTService } from '../../services/rest.service';
import { SecurityService } from '../../services/security.service';

export interface HistoryColumns {
  fieldName: string;
  header: string;
  class?: string;
  mob_header?: string;
}

@Component({
  selector: 'app-visit-history',
  templateUrl: './visit-history.component.html',
  styleUrls: ['./visit-history.component.css']
})
export class VisitHistoryComponent implements OnInit {
  visitData: any[];
  columns: HistoryColumns[];
  showClearDialog: boolean;

  private recs: number;

  constructor(
    private _rest: RESTService,
    private _toast: ToastrService,
    private _security: SecurityService
  ) {
    this.recs = 20;
    this.showClearDialog = false;

    this.columns = [
      { fieldName: 'name', header: 'Пользователь', mob_header: 'Польз.' },
      { fieldName: 'event', header: 'Событие' },
      { fieldName: 'event_time', header: 'Время' }
    ];
  }

  ngOnInit() {
    this.loadData();
  }

  getMore() {
    this.recs += 20;
    this.loadData();
  }

  clearHistory() {
    this.showClearDialog = true;
  }

  doHistoryClear = async () => {
    try {
      await this._rest.postFormData(['activity', 'clearVisits'], {
        function: 'clearVisitInfo'
      });
      this._toast.success('Журнал очищен');
      this.loadData();
      this._security.getVisitCount();
    } catch (e) {}
  }

  private loadData = async () => {
    try {
      const data = await this._rest.postFormData(['activity', 'visits'], {
        records: this.recs
      });
      this.visitData = data.data || [];
    } catch (e) {}
  }
}

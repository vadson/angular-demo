<?php
require_once '../../config/config.php';
require_once '../security/security_mod.php';
require_once '../functions.php';

function restEntry() {
    if (!SiteLogin($GLOBALS["REST_APP_NAME"])) {
        return false;
    }
    return true;
}
?>

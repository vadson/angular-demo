<?php
//
// ��������� !!!!!!!!!!!!!!!!
//
require_once '../api/rest_entry.php';

if (!restEntry()) {
    exit();
}

if (isset($_POST["pass"])) {
    calculateHash();
}

function calculateHash()
{
    $response = array("error" => "N", "error_message" => "","hash" => "", "user" =>$_SERVER['PHP_AUTH_USER']);

    if (isSupervisorLogin()) {
        $response['hash'] = md5($_POST['pass']);
    } else {
        $response['error'] = 'Y';
        $response['error_message'] = "Unauthorised user";
    }

    echo json_encode($response);
}

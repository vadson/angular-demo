<?php


/* Site v2 */
function SetupReportDate() {

	global $year;
	global $mon;

	if(isset($_GET["y"])) {
		$year =  mysql_escape_string($_GET["y"]);
	}
	else {
		$date = GetDate();
		$year = $date["year"];
	}

	if(isset($_GET["m"])) {
		$mon =  mysql_escape_string($_GET["m"]);
	}
	else {
		$date = GetDate();
		$mon = $date["mon"];
	}
}

function GetPageNavLink($page_file,$year,$month) {

	if($month!='') {
		$params = "?y=".$year."&m=".$month;
	}
	else {
		$params = "?y=".$year;
	}
	return $page_file.$params;
}

function GetNextMonth($year,$month) {

	$new_date['month'] = $month+1;
	$new_date['year'] = $year;
	if ($new_date['month'] == 13)
	{
		$new_date['year'] = $year+1;
		$new_date['month'] = 1;
	}
	return $new_date;
}

function GetPrevMonth($year,$month) {

	$new_date['month'] = $month-1;
	$new_date['year'] = $year;
	if ($new_date['month'] == 0)
	{
		$new_date['year'] = $year-1;
		$new_date['month'] = 12;
	}
	return $new_date;
}


?>
